/**
 * Adds two numbers together
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const add = (a, b) => a + b;

/**
 * Subtracts subtrahend from minuend
 * @param {number} minuend 
 * @param {number} subtrahend 
 * @returns {number}
 */
const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};


/**
 * Multiplies two numbers
 * @param {number} multiplier 
 * @param {number} multiplicant 
 * @returns {number}
 */
const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
};

/**
 * Divides two numbers
 * @param {number} dividend 
 * @param {number} divisor 
 * @returns {number}
 * @throws {Error} 0 division
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error ("0 division not allowed");
    const fraction = dividend / divisor;
    return fraction;
};

export default { add, subtract, multiply, divide }
