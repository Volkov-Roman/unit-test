import { expect, should } from 'chai';
import { describe } from 'mocha';
import calc from "../src/calc.js";
import { assert } from 'console';

describe('calc.js', () => {
    let counter;
    before(() => {
        counter = 0;
    });
    beforeEach(() => counter++);
    after(() => console.log(`counter: ${counter}`));
    it("can add numbers", () => {
        expect(calc.add(2, 2)).to.equal(4);
        expect(calc.add(7, 3)).to.equal(10);
    });
    it("can subtract numbers", () => {
        assert(calc.subtract(30, 2) === 28);
        assert(calc.subtract(0.7, 0.3) === 0.4);
    });
    it("can multiply numbers", () => {
        should().exist(calc.multiply);
        calc.multiply(9, 3).should.equal(27);
    });
    it("can divide numbers", () => {
        should().exist(calc.divide);
        expect(calc.divide(2, 4)).to.equal(0.5);
    });
    it("0 division throws an error", () => {
        const err_msg = "0 division not allowed";
        expect(() => calc.divide(1, 0))
            .to
            .throw(err_msg);
    });
});